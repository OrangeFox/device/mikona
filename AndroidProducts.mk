#
# SPDX-License-Identifier: Apache-2.0
#
# Copyright (C) 2021-2023 The OrangeFox Recovery Project
# SPDX-License-Identifier: GPL-3.0-or-later
#

PRODUCT_MAKEFILES := \
	$(LOCAL_DIR)/twrp_alioth.mk \
	$(LOCAL_DIR)/twrp_munch.mk

COMMON_LUNCH_CHOICES := \
	twrp_alioth-eng \
	twrp_munch-eng
#
